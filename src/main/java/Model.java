
class Model {
	
	public Polinom Conversie(String s) {
			String y=s.substring(0,1);
			if(y.equals("-")) s=s.substring(1,s.length());
			s=s.replace("+","a");
			s=s.replace("-","a-");
			String parts1[]=s.split("a");
			if(y.equals("-")) parts1[0]="-"+parts1[0];
			Polinom rez=new Polinom();
			for(String contor:parts1) {
					String parts2[]=contor.split("x");
					String s2=parts2[0].substring(0,parts2[0].length());
					int a1=Integer.parseInt(s2);
					String s3=parts2[1].substring(1,parts2[1].length());
					int a2=Integer.parseInt(s3);
					Monom m1=new Monom(a1,a2);
					rez.add(m1);
				}
			return rez;
	}
	}
