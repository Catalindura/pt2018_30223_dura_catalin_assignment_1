
import static org.junit.Assert.*;
import org.junit.*; 
public class JunitTest {
	private static Polinom p;
	private static int NumarTesteExecutate=0;
	private static int NumarTesteCuSucces=0;
	
	
	public JunitTest() {
		System.out.println("Constructor inaintea fiecarui test");
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	 System.out.println("O singura data inaintea executiei setului de teste din clasa!");
	 p = new Polinom();
	 }
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	 System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
	 System.out.println("S-au executat " + NumarTesteExecutate + " teste din care "+ NumarTesteCuSucces + " au avut succes!");
	 }
	@Before
	public void setUp() throws Exception {
	 System.out.println("Incepe un nou test!");
	 NumarTesteExecutate++;
	 }
	@After
	public void tearDown() throws Exception {
	 System.out.println("S-a terminat testul curent!");
}
	@Test
	public void testAdaugareInPolinom() {
		p.p.removeAll(p.p);
		Monom m=new Monom(2,3);
		Monom m1=new Monom(-2,2);
		p.add(m);
		p.add(m1);
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"+2,0x^3,0-2,0x^2,0");
		NumarTesteCuSucces++;
	}	
	@Test
	public void testAdunare() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(2,3);
		Monom m2=new Monom(-2,2);
		Monom m3=new Monom(2,2);
		Monom m4=new Monom(4,3);
		Polinom p1=new Polinom();
		p.add(m1);
		p.add(m2);
		p1.add(m3);
		p1.add(m4);
		p=p.adunare(p1);
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"+6,0x^3,0");
		NumarTesteCuSucces++;
	}	
	@Test
	public void testScadere() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(2,3);
		Monom m2=new Monom(-2,2);
		Monom m3=new Monom(2,2);
		Monom m4=new Monom(4,3);
		Polinom p1=new Polinom();
		p.add(m1);
		p.add(m2);
		p1.add(m3);
		p1.add(m4);
		p=p.scadere(p1);
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"-2,0x^3,0-4,0x^2,0");
		NumarTesteCuSucces++;
	}	
	@Test
	public void testInmultire() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(1,2);
		Monom m2=new Monom(3,0);
		Monom m3=new Monom(1,2);
		Monom m4=new Monom(3,0);
		Polinom p1=new Polinom();
		p.add(m1);
		p.add(m2);
		p1.add(m3);
		p1.add(m4);
		p=p.inmultire(p1);
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"+x^4,0+6,0x^2,0+9,0x^0,0");
		NumarTesteCuSucces++;
	}
	@Test
	public void testImpartire() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(1,4);
		Monom m2=new Monom(3,0);
		Monom m3=new Monom(1,2);
		Monom m4=new Monom(2,0);
		Polinom p1=new Polinom();
		Polinom rest=new Polinom();
		p.add(m1);
		p.add(m2);
		p1.add(m3);
		p1.add(m4);
		p=p.impartire(p1,rest);
		String cat=p.toString();
		String res=rest.toString();
		assertNotNull(cat);
		assertNotNull(res);
		assertEquals(cat,"+x^2,0-2,0x^0,0");
		assertEquals(res,"+7,0x^0,0");
		NumarTesteCuSucces++;
	}	
	@Test
	public void testDerivare() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(5,5);
		Monom m2=new Monom(3,4);
		p.add(m1);
		p.add(m2);
		p=p.derivare();
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"+25,0x^4,0+12,0x^3,0");
		NumarTesteCuSucces++;
	}
	@Test
	public void testIntegrare() {
		p.p.removeAll(p.p);
		Monom m1=new Monom(-3,2);
		Monom m2=new Monom(-3,1);
		p.add(m1);
		p.add(m2);
		p=p.integrare();
		String s=p.toString();
		assertNotNull(s);
		assertEquals(s,"-x^3,0-1,5x^2,0");
		NumarTesteCuSucces++;
	}
}
