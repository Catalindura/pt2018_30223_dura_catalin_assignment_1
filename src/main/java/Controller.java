
import java.awt.event.*;
class Controller {
	private Interfata interfata=new Interfata();
	private Model model;
	Controller(Model m,Interfata t){
		interfata=t;
		model=m;
		interfata.AdunareListener(new AdunareListener());
		interfata.ScadereListener(new ScadereListener());
		interfata.DerivareListener(new DerivareListener());
		interfata.InmultireListener(new InmultireListener());
		interfata.IntegrareListener(new IntegrareListener());
		interfata.ClearListener(new ClearListener());
		interfata.ImpartireListener(new ImpartireListener());
	}
	class AdunareListener implements ActionListener {
		 public void actionPerformed ( ActionEvent e) {
			 String s1=interfata.getInput1();
			 String s2=interfata.getInput2();
			 Polinom p1=new Polinom();
			 Polinom p2=new Polinom();
			 p1=model.Conversie(s1);
			 p2=model.Conversie(s2);
			 p1=p1.adunare(p2);
			 p1=p1.ordonare();
			 
			 interfata.setArea1(p1.toString());
		 }
		 }
	class ScadereListener implements ActionListener {
		 public void actionPerformed ( ActionEvent e) {
			 String s1=interfata.getInput1();
			 String s2=interfata.getInput2();
			 Polinom p1=new Polinom();
			 Polinom p2=new Polinom();
			 p1=model.Conversie(s1);
			 p2=model.Conversie(s2);
			 p1=p1.scadere(p2);
			 p1=p1.ordonare();
			 interfata.setArea1(p1.toString());
		 }
		 }
	class DerivareListener implements ActionListener {
		 public void actionPerformed ( ActionEvent e) {
			 String s1=interfata.getInput1();
			 Polinom p1=new Polinom();
			 p1=model.Conversie(s1);
			 p1=p1.derivare();
			 p1=p1.ordonare();
			 interfata.setArea1(p1.toString());
		 }
		 }
	class InmultireListener implements ActionListener {
		public void actionPerformed ( ActionEvent e) {
			String s1=interfata.getInput1();
			String s2=interfata.getInput2();
			Polinom p1=new Polinom();
			Polinom p2=new Polinom();
			p1=model.Conversie(s1);
			p2=model.Conversie(s2);
			p1=p1.inmultire(p2);
			p1=p1.ordonare();
		 interfata.setArea1(p1.toString());
	 }
}
	class IntegrareListener implements ActionListener {
		 public void actionPerformed ( ActionEvent e) {
			 String s1=interfata.getInput1();
			 Polinom p1=new Polinom();
			 p1=model.Conversie(s1);
			 p1=p1.integrare();
			 p1=p1.ordonare();
			 interfata.setArea1(p1.toString());
		 }
}
	class ClearListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			interfata.setArea1("");
			interfata.setArea2("");
			interfata.setText1();
			interfata.setText2();
		}
	}
	class ImpartireListener implements ActionListener {
		 public void actionPerformed ( ActionEvent e) {
			    String s1=interfata.getInput1();
				String s2=interfata.getInput2();
				Polinom p1=new Polinom();
				Polinom p2=new Polinom();
				Polinom rest=new Polinom();
				Polinom cat=new Polinom();
				p1=model.Conversie(s1);
				p2=model.Conversie(s2);
				cat=p1.impartire(p2,rest);
				cat=cat.ordonare();
				rest=rest.ordonare();
				interfata.setArea1("Catul este:"+cat.toString());
				interfata.setArea2("Restul este:"+rest.toString());
		 }
	
}
}
