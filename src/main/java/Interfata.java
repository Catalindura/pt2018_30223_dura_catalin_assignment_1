
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class Interfata extends JFrame{
	private JTextField input1=new JTextField(20);
	private JTextField input2=new JTextField(20);
	private JTextArea area1=new JTextArea(4,20);
	private JTextArea area2=new JTextArea(4,20);
	private JButton adunare=new JButton("+");
	private JButton scadere=new JButton("-");
	private JButton inmultire=new JButton("*");
	private JButton impartire=new JButton("/");
	private JButton derivare=new JButton("`");
	private JButton integrare=new JButton("Integrare");
	private JButton clear=new JButton("Clear");
	
	Interfata(){
		JPanel pp=new JPanel();
		JPanel p=new JPanel();
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		JLabel l1=new JLabel("Primul Polinom");
		JLabel l2=new JLabel("Al doilea Polinom");
		p.setLayout(new FlowLayout());
		pp. setLayout (new BoxLayout (pp, BoxLayout . Y_AXIS ));
		p.add(l1);
		p.add(input1);
		p2.setLayout(new FlowLayout());
		p2.add(l2);
		p3.add(area1);
		p4.add(area2);
		p2.add(input2);
		p1.add(adunare);
		p1.add(scadere);
		p1.add(inmultire);
		p1.add(impartire);
		p1.add(derivare);
		p1.add(integrare);
		p1.add(clear);
		pp.add(p);
		pp.add(p2);
		pp.add(p3);
		pp.add(p1);
		pp.add(p4);
		this.setSize(480,640);
		this.add(pp);
		this.pack();
		this.setTitle("Operatii pe Polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public String getInput1() {
		return input1.getText();
	}
	public String getInput2() {
		return input2.getText();
	}
	public void setArea1(String x) {
		area1.setText(x);
	}
	public void setArea2(String x) {
		area2.setText(x);
	}
	public void setText1() {
		input1.setText("");
	}
	public void setText2() {
		input2.setText("");
	}
	void AdunareListener (ActionListener mal) {
		 adunare.addActionListener(mal);
	}
	void ScadereListener (ActionListener mal) {
		 scadere.addActionListener(mal);
	}
	void DerivareListener(ActionListener mal) {
		derivare.addActionListener(mal);
	}
	void InmultireListener(ActionListener mal) {
		inmultire.addActionListener(mal);
	}
	void IntegrareListener(ActionListener mal) {
		integrare.addActionListener(mal);
	}
	void ClearListener(ActionListener mal) {
		clear.addActionListener(mal);
	}
	void ImpartireListener(ActionListener mal) {
		impartire.addActionListener(mal);
	}
}