
import java.util.*;
import java.lang.Math;
class Polinom {
	ArrayList<Monom> p=new ArrayList<Monom>();
	public void add(Monom x) {
		p.add(x);
	}
	public String toString() {
		String z="";
		for(Monom m:p) {
			z=z+m.toString();
		}
		return z;
		}
	public Monom getMonomMax() {
		Monom rez=new Monom(0,-1);
		for(Monom m:p) {
			if((m.getPutere()>rez.getPutere())&&(m.getCoef()!=0)) {
				rez.setPutere(m.getPutere());
				rez.setCoef(m.getCoef());
			}
		}
		return rez;
	}
	public Polinom adunare(Polinom p1) {
		Polinom rez1=new Polinom();
		Polinom aux=new Polinom();
		aux.p.addAll(p1.p);
		for(Monom m1:p) {
			int ok=1;
			for(Monom m2:p1.p) {
				if(m1.getPutere()==m2.getPutere()) {
					int i=0;
					int nr=0;
					for(Monom x:aux.p) {
						if(x.getPutere()==m2.getPutere())
							nr=i;
						i=i+1;
					}
					ok=0;
					Monom z=new Monom(m1.getCoef()+m2.getCoef(),m1.getPutere());
					rez1.add(z);
					aux.p.remove(nr);
				}
			}
			if(ok==1) {
				Monom z1=new Monom(m1.getCoef(),m1.getPutere());
				rez1.add(z1);
			}
		}
		for(Monom m2:aux.p) {
			Monom m=new Monom(m2.getCoef(),m2.getPutere());
			rez1.add(m);
		}
		return rez1;
	}
	
	public Polinom scadere(Polinom p1) {
		Polinom rez1=new Polinom();
		Polinom aux=new Polinom();
		aux.p.addAll(p1.p);
		for(Monom m1:p) {
			int ok=1;
			for(Monom m2:p1.p) {
				int i=0;
				int nr=0;
				for(Monom x:aux.p) {
					if(x.getPutere()==m2.getPutere())
						nr=i;
					i=i+1;
				}
				if(m1.getPutere()==m2.getPutere()) {
					ok=0;
					Monom z=new Monom(m1.getCoef()-m2.getCoef(),m1.getPutere());
					rez1.add(z);
					aux.p.remove(nr);
					
					
				}
				else i=i+1;
			}
			if(ok==1) {
				Monom z1=new Monom(m1.getCoef(),m1.getPutere());
				rez1.add(z1);
			}
		}
		for(Monom m2:aux.p) {
			Monom m=new Monom(-m2.getCoef(),m2.getPutere());
			rez1.add(m);
		}
		return rez1;
	}
	
	public Polinom derivare() {
		Polinom aux=new Polinom();
		for(Monom d:p) {
			d=d.derivare();
			aux.add(d);
		}
		return aux;
	}
	public Polinom integrare() {
		Polinom aux=new Polinom();
		for(Monom d:p) {
			d=d.integrare();
			aux.add(d);
		}
		return aux;
	}
	public Polinom inmultire(Polinom p1) {
		Polinom rez=new Polinom();
		for(Monom m1:p) {
			Polinom aux=new Polinom();
			for(Monom m2:p1.p) {
				Monom re=new Monom(m1.getCoef()*m2.getCoef(),m1.getPutere()+m2.getPutere());
				aux.add(re);
			}
			rez=rez.adunare(aux);
		}
		return rez;
	}
	public Polinom ordonare() {
		Polinom rez=new Polinom();
		Polinom aux=new Polinom();
		aux.p.addAll(p);
		for(Monom m:p) {
			Monom m1=aux.getMonomMax();
			int pos=0;
			int nr=0;
			for(Monom iterator:aux.p) {
				if(iterator.getPutere()==m1.getPutere()) pos=nr;
				nr=nr+1;
			}
			System.out.println(pos);
			rez.add(m1);
			aux.p.remove(pos);
		}
		return rez;
	}
	public Polinom inmultirecuMonom(Monom m) {
		Polinom rez=new Polinom();
		for(Monom m1:p) {
			Monom aux=new Monom(m1.getCoef()*m.getCoef(),m1.getPutere()+m.getPutere());
			rez.add(aux);
		}
		return rez;
	}
	public Polinom impartire(Polinom i,Polinom r) {
		Polinom cat=new Polinom();
		Polinom d=new Polinom();
		d.p.addAll(p);
		Monom m1=d.getMonomMax();
		Monom m2=i.getMonomMax();
		while(m1.getPutere()>m2.getPutere()) {
			Monom m3=m1.divide(m2);
			cat.add(m3);
			Polinom aux=i.inmultirecuMonom(m3);
			d=d.scadere(aux);
			m1=d.getMonomMax();
		}
		Monom m3=m1.divide(m2);
		cat.add(m3);
		Polinom aux=i.inmultirecuMonom(m3);
		d=d.scadere(aux);
		m1=d.getMonomMax();
		r.p.addAll(d.p);
		return cat;
		
	}
	}
