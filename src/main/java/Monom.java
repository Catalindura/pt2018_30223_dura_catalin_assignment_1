

import java.text.*;
class Monom {
	private double coef;
	private double putere;
	public Monom(double x,double y) {
		coef=x;
		putere=y;
	}
	public Monom divide(Monom m) {
		Monom rez=new Monom(0,0);
		if(coef<0 ||m.getCoef()<0) 
			rez=new Monom((int)(coef/m.getCoef()),putere-m.getPutere());
		else rez=new Monom((int)(coef/m.getCoef()),putere-m.getPutere());
		return rez;
	}
	public void setCoef(double x) {
		coef=x;
	}
	public void setPutere(double x) {
		putere=x;
	}
	public double getCoef() {
		return coef;
	}
	public double getPutere() {
		return putere;
	}
	public Monom derivare() {
		Monom z=new Monom(coef*putere,putere-1);
		return z;
	}
	public Monom integrare() {
		Monom z=new Monom(coef/(putere+1),putere+1);
		return z;
	}
	public String toString() {
		DecimalFormat format=new DecimalFormat("#0.0");
		if(coef==1)
			return "+x^"+format.format(putere);
		else
			if(coef==-1) return "-x^"+format.format(putere);
		else
			if (coef==0) return "";
		else
			if(coef>0)
				return "+"+format.format(coef)+"x^"+format.format(putere);
			else return format.format(coef)+"x^"+format.format(putere);
	}
}
